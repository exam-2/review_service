package messagebroker

type Produce interface {
	Start() error
	Stop() error
	Produce(key, body []byte, logBody string) error
}
