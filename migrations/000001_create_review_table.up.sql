CREATE TABLE IF NOT EXISTS review (
    id serial PRIMARY KEY,
    customer_id INT,
    post_id INT,
    description TEXT,
    rating INT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
)