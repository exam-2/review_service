package repo

import (
	"temp/genproto/post"
	pr "temp/genproto/review"
)

type ReviewStorageI interface{
	// GetReviewAVG(*pr.ReviewReq) (*pr.ReviewResp, error)
	CreateReview(*pr.ReviewReq) (*pr.ReviewResp, error)
	CreatePost(*post.PostResp) error
	UpdateReview(*pr.ReviewResp) (*pr.ReviewResp, error)
	GetReviewByPostID(*pr.ReviewID) (*pr.ReviewList, error) 
}
