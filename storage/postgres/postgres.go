package postgres

import "github.com/jmoiron/sqlx"

type reviewRepo struct{
	Db *sqlx.DB
}

func NewReviewRepo(db *sqlx.DB) *reviewRepo{
	return &reviewRepo{Db:	db}
}

