package postgres

import (
	"fmt"
	"temp/genproto/post"
	pr "temp/genproto/review"
)

func (r *reviewRepo) CreateReview(req *pr.ReviewReq) (*pr.ReviewResp, error) {
	responce := &pr.ReviewResp{}
	err := r.Db.QueryRow(`insert into review(customer_id, post_id, rating, description) values($1, $2, $3, $4) 
	returning id, customer_id, post_id, rating,description`, req.CustomerId, req.PostId, req.Rating, req.Description,
	).Scan(&responce.Id, &responce.CustomerId, &responce.PostId, &responce.Rating, &responce.Description)
	if err != nil {
		return &pr.ReviewResp{}, err
	}

	return responce, nil
}

func (r *reviewRepo) CreatePost(req *post.PostResp) error {
	fmt.Println(`INSERTING`)
	responce := &post.PostResp{}

	err := r.Db.QueryRow(`INSERT INTO post_review(customer_id, name, description, about) 
	VALUES($1, $2, $3, $4) 
	returning id, customer_id, name, description, about`,
		req.CustomerId,
		req.Name,
		req.Description,
		req.About).Scan(
		&responce.Id,
		&responce.CustomerId,
		&responce.Name,
		&responce.Description,
		&responce.About,
	)
	if err != nil {
		return err
	}
	fmt.Println(&responce.Name)
	fmt.Println(&responce.Description)
	fmt.Println(&responce.About)
	for _, media := range req.Media {

		media.PostId = responce.Id
		err := r.CreateMedia(media)
		if err != nil {
			return err
		}

		responce.Media = append(responce.Media, media)
	}
	fmt.Println("Successs				`")	

	return nil
}

func (r *reviewRepo) UpdateReview(req *pr.ReviewResp) (*pr.ReviewResp, error) {
	err := r.Db.QueryRow(`update review SET updated_at=NOW(),customer_id=$1, post_id=$2, rating=$3, description=$4 where id=$5 
	and deleted_at is null`, req.CustomerId, req.PostId, req.Rating, req.Description, req.Id).Err()
	if err != nil {
		return nil, err
	}
	return req, nil
}

func (r *reviewRepo) GetReviewByPostID(req *pr.ReviewID) (*pr.ReviewList, error) {
	rows, err := r.Db.Query(`select id, customer_id, post_id, description, rating from review where post_id=$1 and deleted_at is null`, req.ReviewID)
	if err != nil {
		return &pr.ReviewList{}, err
	}
	defer rows.Close()
	reviews := &pr.ReviewList{}
	for rows.Next() {
		temp := &pr.ReviewResp{}
		err = rows.Scan(&temp.Id, &temp.CustomerId, &temp.PostId, &temp.Description, &temp.Rating)
		if err != nil {
			return nil, err
		}
		reviews.Reviews = append(reviews.Reviews, temp)
	}
	return reviews, nil
}

func (r *reviewRepo) CreateMedia(req *post.Media) error {
	return r.Db.QueryRow(`INSERT INTO media(post_id, name, link, type) VALUES($1, $2, $3, $4)`, req.PostId, req.Name, req.Link, req.Type).Err()
}
