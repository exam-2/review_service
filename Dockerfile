FROM golang:1.19.3-alpine
RUN mkdir review_service
COPY . /review_service
WORKDIR /review_service
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 1111