package main

import (
	"net"
	"temp/config"
	pr "temp/genproto/review"
	"temp/pkg/db"
	"temp/pkg/logger"
	"temp/services"
	"temp/services/client"
	"temp/kafka"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "review-service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))
	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	client, err := client.New(cfg)
	if err != nil {
		log.Fatal(`error while connect to clients post`, logger.Error(err))
	}

	PostCreateTopic := kafka.NewKafkaConsumer(connDB, &cfg, log, "post.post")
	go PostCreateTopic.Start()

	productService := services.NewReviewService(connDB, log, client)

	lis, err := net.Listen("tcp",":" + cfg.ReviewServicePort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	
	s := grpc.NewServer()
	reflection.Register(s)
	
	pr.RegisterReviewServiceServer(s, productService)
	log.Info("main: server running",
		logger.String("port", cfg.ReviewServicePort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
