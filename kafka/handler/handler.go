package kafka

import (
	"temp/config"
	"temp/genproto/post"
	"temp/pkg/logger"
	"temp/storage"
)

type KafkaHandler struct {
	config  config.Config
	storage storage.IStorage
	log     logger.Logger
}

func NewKafkaHandlerFunc(config config.Config, storage storage.IStorage, log logger.Logger) *KafkaHandler {
	return &KafkaHandler{
		config:  config,
		storage: storage,
		log:     log,
	}
}

func (k *KafkaHandler) Handle(value []byte) error {
	user := post.PostResp{}
	err := user.Unmarshal(value)
	if err != nil {
		return err
	}

	err = k.storage.Review().CreatePost(&user)
	if err != nil {
		return 	err
	}
	return nil 
}