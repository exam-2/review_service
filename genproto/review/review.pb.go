// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: review/review.proto

package review

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ReviewList struct {
	Reviews              []*ReviewResp `protobuf:"bytes,1,rep,name=reviews,proto3" json:"reviews"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *ReviewList) Reset()         { *m = ReviewList{} }
func (m *ReviewList) String() string { return proto.CompactTextString(m) }
func (*ReviewList) ProtoMessage()    {}
func (*ReviewList) Descriptor() ([]byte, []int) {
	return fileDescriptor_63b26697b52db059, []int{0}
}
func (m *ReviewList) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReviewList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReviewList.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReviewList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReviewList.Merge(m, src)
}
func (m *ReviewList) XXX_Size() int {
	return m.Size()
}
func (m *ReviewList) XXX_DiscardUnknown() {
	xxx_messageInfo_ReviewList.DiscardUnknown(m)
}

var xxx_messageInfo_ReviewList proto.InternalMessageInfo

func (m *ReviewList) GetReviews() []*ReviewResp {
	if m != nil {
		return m.Reviews
	}
	return nil
}

type ReviewID struct {
	ReviewID             int64    `protobuf:"varint,1,opt,name=ReviewID,proto3" json:"ReviewID"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReviewID) Reset()         { *m = ReviewID{} }
func (m *ReviewID) String() string { return proto.CompactTextString(m) }
func (*ReviewID) ProtoMessage()    {}
func (*ReviewID) Descriptor() ([]byte, []int) {
	return fileDescriptor_63b26697b52db059, []int{1}
}
func (m *ReviewID) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReviewID) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReviewID.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReviewID) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReviewID.Merge(m, src)
}
func (m *ReviewID) XXX_Size() int {
	return m.Size()
}
func (m *ReviewID) XXX_DiscardUnknown() {
	xxx_messageInfo_ReviewID.DiscardUnknown(m)
}

var xxx_messageInfo_ReviewID proto.InternalMessageInfo

func (m *ReviewID) GetReviewID() int64 {
	if m != nil {
		return m.ReviewID
	}
	return 0
}

type ReviewReq struct {
	CustomerId           string   `protobuf:"bytes,1,opt,name=customer_id,json=customerId,proto3" json:"customer_id"`
	PostId               string   `protobuf:"bytes,2,opt,name=post_id,json=postId,proto3" json:"post_id"`
	Rating               int64    `protobuf:"varint,3,opt,name=rating,proto3" json:"rating"`
	Description          string   `protobuf:"bytes,4,opt,name=description,proto3" json:"description"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReviewReq) Reset()         { *m = ReviewReq{} }
func (m *ReviewReq) String() string { return proto.CompactTextString(m) }
func (*ReviewReq) ProtoMessage()    {}
func (*ReviewReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_63b26697b52db059, []int{2}
}
func (m *ReviewReq) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReviewReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReviewReq.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReviewReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReviewReq.Merge(m, src)
}
func (m *ReviewReq) XXX_Size() int {
	return m.Size()
}
func (m *ReviewReq) XXX_DiscardUnknown() {
	xxx_messageInfo_ReviewReq.DiscardUnknown(m)
}

var xxx_messageInfo_ReviewReq proto.InternalMessageInfo

func (m *ReviewReq) GetCustomerId() string {
	if m != nil {
		return m.CustomerId
	}
	return ""
}

func (m *ReviewReq) GetPostId() string {
	if m != nil {
		return m.PostId
	}
	return ""
}

func (m *ReviewReq) GetRating() int64 {
	if m != nil {
		return m.Rating
	}
	return 0
}

func (m *ReviewReq) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

type ReviewResp struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id"`
	CustomerId           string   `protobuf:"bytes,2,opt,name=customer_id,json=customerId,proto3" json:"customer_id"`
	PostId               string   `protobuf:"bytes,3,opt,name=post_id,json=postId,proto3" json:"post_id"`
	Description          string   `protobuf:"bytes,4,opt,name=description,proto3" json:"description"`
	Rating               int64    `protobuf:"varint,5,opt,name=rating,proto3" json:"rating"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReviewResp) Reset()         { *m = ReviewResp{} }
func (m *ReviewResp) String() string { return proto.CompactTextString(m) }
func (*ReviewResp) ProtoMessage()    {}
func (*ReviewResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_63b26697b52db059, []int{3}
}
func (m *ReviewResp) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReviewResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReviewResp.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReviewResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReviewResp.Merge(m, src)
}
func (m *ReviewResp) XXX_Size() int {
	return m.Size()
}
func (m *ReviewResp) XXX_DiscardUnknown() {
	xxx_messageInfo_ReviewResp.DiscardUnknown(m)
}

var xxx_messageInfo_ReviewResp proto.InternalMessageInfo

func (m *ReviewResp) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ReviewResp) GetCustomerId() string {
	if m != nil {
		return m.CustomerId
	}
	return ""
}

func (m *ReviewResp) GetPostId() string {
	if m != nil {
		return m.PostId
	}
	return ""
}

func (m *ReviewResp) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *ReviewResp) GetRating() int64 {
	if m != nil {
		return m.Rating
	}
	return 0
}

func init() {
	proto.RegisterType((*ReviewList)(nil), "review.ReviewList")
	proto.RegisterType((*ReviewID)(nil), "review.ReviewID")
	proto.RegisterType((*ReviewReq)(nil), "review.ReviewReq")
	proto.RegisterType((*ReviewResp)(nil), "review.ReviewResp")
}

func init() { proto.RegisterFile("review/review.proto", fileDescriptor_63b26697b52db059) }

var fileDescriptor_63b26697b52db059 = []byte{
	// 322 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2e, 0x4a, 0x2d, 0xcb,
	0x4c, 0x2d, 0xd7, 0x87, 0x50, 0x7a, 0x05, 0x45, 0xf9, 0x25, 0xf9, 0x42, 0x6c, 0x10, 0x9e, 0x92,
	0x15, 0x17, 0x57, 0x10, 0x98, 0xe5, 0x93, 0x59, 0x5c, 0x22, 0xa4, 0xc3, 0xc5, 0x0e, 0x11, 0x2f,
	0x96, 0x60, 0x54, 0x60, 0xd6, 0xe0, 0x36, 0x12, 0xd2, 0x83, 0xea, 0x82, 0x28, 0x0a, 0x4a, 0x2d,
	0x2e, 0x08, 0x82, 0x29, 0x51, 0x52, 0xe3, 0xe2, 0x80, 0x08, 0x7b, 0xba, 0x08, 0x49, 0x21, 0xd8,
	0x12, 0x8c, 0x0a, 0x8c, 0x1a, 0xcc, 0x41, 0x70, 0xbe, 0x52, 0x3d, 0x17, 0x27, 0x4c, 0x7b, 0xa1,
	0x90, 0x3c, 0x17, 0x77, 0x72, 0x69, 0x71, 0x49, 0x7e, 0x6e, 0x6a, 0x51, 0x7c, 0x66, 0x0a, 0x58,
	0x2d, 0x67, 0x10, 0x17, 0x4c, 0xc8, 0x33, 0x45, 0x48, 0x9c, 0x8b, 0xbd, 0x20, 0xbf, 0xb8, 0x04,
	0x24, 0xc9, 0x04, 0x96, 0x64, 0x03, 0x71, 0x3d, 0x53, 0x84, 0xc4, 0xb8, 0xd8, 0x8a, 0x12, 0x4b,
	0x32, 0xf3, 0xd2, 0x25, 0x98, 0xc1, 0x16, 0x40, 0x79, 0x42, 0x0a, 0x5c, 0xdc, 0x29, 0xa9, 0xc5,
	0xc9, 0x45, 0x99, 0x05, 0x25, 0x99, 0xf9, 0x79, 0x12, 0x2c, 0x60, 0x4d, 0xc8, 0x42, 0x4a, 0x13,
	0x18, 0x61, 0xbe, 0x04, 0x79, 0x40, 0x88, 0x8f, 0x8b, 0x09, 0x6a, 0x33, 0x73, 0x10, 0x53, 0x66,
	0x0a, 0xba, 0x93, 0x98, 0xf0, 0x39, 0x89, 0x19, 0xc5, 0x49, 0x04, 0xad, 0x46, 0x72, 0x34, 0x2b,
	0xb2, 0xa3, 0x8d, 0x76, 0x32, 0x72, 0xf1, 0x42, 0x9c, 0x14, 0x9c, 0x5a, 0x54, 0x96, 0x99, 0x9c,
	0x2a, 0x64, 0xca, 0xc5, 0xe3, 0x5c, 0x94, 0x9a, 0x58, 0x92, 0x0a, 0x11, 0x16, 0x12, 0x44, 0x0f,
	0xfa, 0x42, 0x29, 0x2c, 0xb1, 0x21, 0x64, 0xc6, 0xc5, 0x13, 0x5a, 0x90, 0x82, 0xd0, 0x86, 0x45,
	0x0d, 0x56, 0x7d, 0x96, 0x5c, 0x82, 0xee, 0xa9, 0x25, 0x10, 0x01, 0xa7, 0xca, 0x00, 0x90, 0x7f,
	0x5c, 0x84, 0x04, 0x50, 0x15, 0x7a, 0xba, 0xa0, 0x6b, 0x05, 0xa5, 0x12, 0x27, 0x81, 0x13, 0x8f,
	0xe4, 0x18, 0x2f, 0x3c, 0x92, 0x63, 0x7c, 0xf0, 0x48, 0x8e, 0x71, 0xc6, 0x63, 0x39, 0x86, 0x24,
	0x36, 0x70, 0xa2, 0x32, 0x06, 0x04, 0x00, 0x00, 0xff, 0xff, 0x40, 0x65, 0x05, 0x96, 0x6b, 0x02,
	0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ReviewServiceClient is the client API for ReviewService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ReviewServiceClient interface {
	CreateReview(ctx context.Context, in *ReviewReq, opts ...grpc.CallOption) (*ReviewResp, error)
	UpdateReview(ctx context.Context, in *ReviewResp, opts ...grpc.CallOption) (*ReviewResp, error)
	// rpc DeleteReview(ReviewID) returns(ReviewResp);
	GetReviewByPostID(ctx context.Context, in *ReviewID, opts ...grpc.CallOption) (*ReviewList, error)
}

type reviewServiceClient struct {
	cc *grpc.ClientConn
}

func NewReviewServiceClient(cc *grpc.ClientConn) ReviewServiceClient {
	return &reviewServiceClient{cc}
}

func (c *reviewServiceClient) CreateReview(ctx context.Context, in *ReviewReq, opts ...grpc.CallOption) (*ReviewResp, error) {
	out := new(ReviewResp)
	err := c.cc.Invoke(ctx, "/review.ReviewService/CreateReview", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *reviewServiceClient) UpdateReview(ctx context.Context, in *ReviewResp, opts ...grpc.CallOption) (*ReviewResp, error) {
	out := new(ReviewResp)
	err := c.cc.Invoke(ctx, "/review.ReviewService/UpdateReview", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *reviewServiceClient) GetReviewByPostID(ctx context.Context, in *ReviewID, opts ...grpc.CallOption) (*ReviewList, error) {
	out := new(ReviewList)
	err := c.cc.Invoke(ctx, "/review.ReviewService/GetReviewByPostID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ReviewServiceServer is the server API for ReviewService service.
type ReviewServiceServer interface {
	CreateReview(context.Context, *ReviewReq) (*ReviewResp, error)
	UpdateReview(context.Context, *ReviewResp) (*ReviewResp, error)
	// rpc DeleteReview(ReviewID) returns(ReviewResp);
	GetReviewByPostID(context.Context, *ReviewID) (*ReviewList, error)
}

// UnimplementedReviewServiceServer can be embedded to have forward compatible implementations.
type UnimplementedReviewServiceServer struct {
}

func (*UnimplementedReviewServiceServer) CreateReview(ctx context.Context, req *ReviewReq) (*ReviewResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateReview not implemented")
}
func (*UnimplementedReviewServiceServer) UpdateReview(ctx context.Context, req *ReviewResp) (*ReviewResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateReview not implemented")
}
func (*UnimplementedReviewServiceServer) GetReviewByPostID(ctx context.Context, req *ReviewID) (*ReviewList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetReviewByPostID not implemented")
}

func RegisterReviewServiceServer(s *grpc.Server, srv ReviewServiceServer) {
	s.RegisterService(&_ReviewService_serviceDesc, srv)
}

func _ReviewService_CreateReview_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReviewReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewServiceServer).CreateReview(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/review.ReviewService/CreateReview",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewServiceServer).CreateReview(ctx, req.(*ReviewReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ReviewService_UpdateReview_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReviewResp)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewServiceServer).UpdateReview(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/review.ReviewService/UpdateReview",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewServiceServer).UpdateReview(ctx, req.(*ReviewResp))
	}
	return interceptor(ctx, in, info, handler)
}

func _ReviewService_GetReviewByPostID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReviewID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewServiceServer).GetReviewByPostID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/review.ReviewService/GetReviewByPostID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewServiceServer).GetReviewByPostID(ctx, req.(*ReviewID))
	}
	return interceptor(ctx, in, info, handler)
}

var _ReviewService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "review.ReviewService",
	HandlerType: (*ReviewServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateReview",
			Handler:    _ReviewService_CreateReview_Handler,
		},
		{
			MethodName: "UpdateReview",
			Handler:    _ReviewService_UpdateReview_Handler,
		},
		{
			MethodName: "GetReviewByPostID",
			Handler:    _ReviewService_GetReviewByPostID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "review/review.proto",
}

func (m *ReviewList) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReviewList) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReviewList) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.Reviews) > 0 {
		for iNdEx := len(m.Reviews) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Reviews[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintReview(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0xa
		}
	}
	return len(dAtA) - i, nil
}

func (m *ReviewID) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReviewID) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReviewID) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if m.ReviewID != 0 {
		i = encodeVarintReview(dAtA, i, uint64(m.ReviewID))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *ReviewReq) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReviewReq) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReviewReq) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.Description) > 0 {
		i -= len(m.Description)
		copy(dAtA[i:], m.Description)
		i = encodeVarintReview(dAtA, i, uint64(len(m.Description)))
		i--
		dAtA[i] = 0x22
	}
	if m.Rating != 0 {
		i = encodeVarintReview(dAtA, i, uint64(m.Rating))
		i--
		dAtA[i] = 0x18
	}
	if len(m.PostId) > 0 {
		i -= len(m.PostId)
		copy(dAtA[i:], m.PostId)
		i = encodeVarintReview(dAtA, i, uint64(len(m.PostId)))
		i--
		dAtA[i] = 0x12
	}
	if len(m.CustomerId) > 0 {
		i -= len(m.CustomerId)
		copy(dAtA[i:], m.CustomerId)
		i = encodeVarintReview(dAtA, i, uint64(len(m.CustomerId)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *ReviewResp) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReviewResp) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReviewResp) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if m.Rating != 0 {
		i = encodeVarintReview(dAtA, i, uint64(m.Rating))
		i--
		dAtA[i] = 0x28
	}
	if len(m.Description) > 0 {
		i -= len(m.Description)
		copy(dAtA[i:], m.Description)
		i = encodeVarintReview(dAtA, i, uint64(len(m.Description)))
		i--
		dAtA[i] = 0x22
	}
	if len(m.PostId) > 0 {
		i -= len(m.PostId)
		copy(dAtA[i:], m.PostId)
		i = encodeVarintReview(dAtA, i, uint64(len(m.PostId)))
		i--
		dAtA[i] = 0x1a
	}
	if len(m.CustomerId) > 0 {
		i -= len(m.CustomerId)
		copy(dAtA[i:], m.CustomerId)
		i = encodeVarintReview(dAtA, i, uint64(len(m.CustomerId)))
		i--
		dAtA[i] = 0x12
	}
	if m.Id != 0 {
		i = encodeVarintReview(dAtA, i, uint64(m.Id))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func encodeVarintReview(dAtA []byte, offset int, v uint64) int {
	offset -= sovReview(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *ReviewList) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Reviews) > 0 {
		for _, e := range m.Reviews {
			l = e.Size()
			n += 1 + l + sovReview(uint64(l))
		}
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *ReviewID) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.ReviewID != 0 {
		n += 1 + sovReview(uint64(m.ReviewID))
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *ReviewReq) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.CustomerId)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	l = len(m.PostId)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	if m.Rating != 0 {
		n += 1 + sovReview(uint64(m.Rating))
	}
	l = len(m.Description)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *ReviewResp) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Id != 0 {
		n += 1 + sovReview(uint64(m.Id))
	}
	l = len(m.CustomerId)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	l = len(m.PostId)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	l = len(m.Description)
	if l > 0 {
		n += 1 + l + sovReview(uint64(l))
	}
	if m.Rating != 0 {
		n += 1 + sovReview(uint64(m.Rating))
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func sovReview(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozReview(x uint64) (n int) {
	return sovReview(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *ReviewList) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowReview
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReviewList: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReviewList: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Reviews", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Reviews = append(m.Reviews, &ReviewResp{})
			if err := m.Reviews[len(m.Reviews)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipReview(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthReview
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ReviewID) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowReview
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReviewID: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReviewID: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field ReviewID", wireType)
			}
			m.ReviewID = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.ReviewID |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipReview(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthReview
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ReviewReq) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowReview
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReviewReq: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReviewReq: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field CustomerId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.CustomerId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field PostId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.PostId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Rating", wireType)
			}
			m.Rating = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Rating |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Description", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Description = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipReview(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthReview
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ReviewResp) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowReview
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReviewResp: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReviewResp: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Id", wireType)
			}
			m.Id = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Id |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field CustomerId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.CustomerId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field PostId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.PostId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Description", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthReview
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthReview
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Description = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 5:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Rating", wireType)
			}
			m.Rating = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowReview
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Rating |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipReview(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthReview
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipReview(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowReview
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowReview
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowReview
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthReview
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupReview
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthReview
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthReview        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowReview          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupReview = fmt.Errorf("proto: unexpected end of group")
)
