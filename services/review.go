package services

import (
	"context"
	"fmt"
	pr "temp/genproto/review"
	"temp/pkg/logger"
	"temp/services/client"
	"temp/storage"

	"github.com/jmoiron/sqlx"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ReviewService struct{
	storage storage.IStorage
	logger logger.Logger
	Client client.Clients

}

func NewReviewService(db *sqlx.DB, log logger.Logger, client client.Clients) *ReviewService {
	return &ReviewService{
		storage: storage.NewStoragePg(db),
		logger: log,
		Client: client,
	}
}

func (r *ReviewService) CreateReview(ctx context.Context, req *pr.ReviewReq) (*pr.ReviewResp, error) {
	review, err := r.storage.Review().CreateReview(req)
	if err != nil{
		r.logger.Error("Error while creating rewiew", logger.Any("create", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return review, nil
}

func (r *ReviewService) UpdateReview(ctx context.Context, req *pr.ReviewResp) (*pr.ReviewResp, error) {
	review, err := r.storage.Review().UpdateReview(req)
	if err != nil{
		r.logger.Error("Error while updating rewiew", logger.Any("UPDATE", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return review, nil
}

func (r *ReviewService) GetReviewByPostID(ctx context.Context, req *pr.ReviewID) (*pr.ReviewList, error) {
	fmt.Println("Enter GetReview  ")
	review, err := r.storage.Review().GetReviewByPostID(req)
	if err != nil{
		r.logger.Error("Error while updating review",logger.Any("GETTING",err))
		return &pr.ReviewList{}, status.Error(codes.Internal,"Please check your info")
	}
	fmt.Println("Exit GetReview   ")
	return review, nil
}