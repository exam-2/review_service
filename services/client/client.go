package client

import (
	"fmt"
	"temp/config"
	pp "temp/genproto/post"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	Post() pp.PostServiceClient
}

type ServiceManager struct {
	config      config.Config
	postService pp.PostServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	post, err := grpc.Dial(
		fmt.Sprintf(`%s:%s`, cfg.PostServiceHost, cfg.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}
	
	return &ServiceManager{
		config: cfg,
		postService: pp.NewPostServiceClient(post),
	}, nil
}

func (p *ServiceManager) Post() pp.PostServiceClient {
	return p.postService
}
